# Project Nigel

The purpose of SBIR Phase I proposal is to demonstrate the feasibility of developing software that significantly improves automated data acquisition speed by making better use of hardware, and by allowing imaging of multiplexed sample grids.

* Improve data acquisition efficiency
* Automate imaging of multiplexed grids and create a web-based interface
* Testing and validation

This repository contains code used in part to develop a prototype for Project Nigel. The code was developed by Craig Yoshioka, Lead R&D Developer/Scientist of NanoImaing Services with the assistance of Rise Riyo, R&D Developer.

# Description: _azmq_

_azmq_ is a Python library that sends messages from a client to a server. It allows for messages to be delivered asynchronously by using a limited-size queue of _zmq_ sockets. In essence, _azmq_ implements an asychronous network Remote Procedural Call (RPC).

The client connects to the server via a _zmq_ socket. Each message is serialized and then sent asynchronously via a _greenlet_ to the server.

The server acknowledges each message sent from its clients, spawns a _greenlet_ to handle each message, and places each _greenlet_ on the hub. The number of _greenlets_ on the hub is controlled by the set value of the concurrency of the pool. Each message waits until it can be sent on to the electron microscope.

# Dependencies

_azmq_ contains as dependencies: _pyzmq_, _gevent_, and _msgpack_.

* _pyzmq_ sets up the socket-layers
* _gevent_ handles the _greenlets_ which are simply co-routines
* _msgpack_ serializes/deserializes the messages

Note: See Installation

# Documentation

Not available yet.

# Installation

One can install _azmq_ in the following way:

If [Anaconda](www.continuum.io) or [Miniconda](www.continuum.io) is already installed, use `conda` to install _azmq_.

        conda install --channel https://conda.binstar.org/nigel azmq

Note: Installation of _azmq_ via `conda` will install the dependencies of _azmq_ listed above as well.
