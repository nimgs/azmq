
import msgpack
import zmq.green as zmq

import gevent
from gevent.queue import Queue
from gevent.pool import Pool
from errors import RemoteException
from contextlib import contextmanager

context = zmq.Context()

class Client(object):
	'''
	create a zmq client designed to connect to a corresponding server instance
	at `address`.  This client uses a pool of sockets to prevent needless opening 
	and closing of sockets and to limit the number of open sockets.
	This prevents exceeding system resource limits, and is controlled by `concurrency`.
	'''
	
	def __init__(self, address, concurrency=100):
		'''
		required:
			address: a zmq formatted address that specifies the server
		optional:
			concurrency: the maximum number of open sockets that can be held.
		'''
		self.address  = address
		self.sockets  = Queue()
		self.maxsock  = concurrency
		self.maxretry = 100
	
	def get_idle_socket(self):
		'''
		return an idle socket.  try creating a new one if neccesary but
		if not possible, block the current greenlet until one becomes
		available.
		'''
		return self.try_existing_socket() or self.try_new_socket() or self.wait_for_idle_socket()

	def try_existing_socket(self):
		'''
		returns an idle socket if one already exists in the queue.
		otherwise returns None, rather than waiting for one to
		become available.
		'''
		if self.sockets.qsize():
			return self.sockets.get()
		return None

	def wait_for_idle_socket(self):
		'''
		we block the current greenlet until an idle socket becomes available
		'''
		return self.sockets.get()

	def try_new_socket(self):
		'''
		if possible, create a new socket and return it.
		keep track of number of sockets created.
		'''
		if self.maxsock:
			socket = context.socket(zmq.REQ)
			socket.connect(self.address)
			self.maxsock -= 1
			return socket
		return None

	def recycle_socket(self, socket):
		self.sockets.put(socket)
		#self.idling.release()

	def request(self, *args, **kwargs):
		'''
		grabs an idle socket and makes a request.
		properly maps any exceptions on the server end
		to an exception in the client.
		if this function raises a ZMQError it will be caught
		and another try will be made.
		'''
		tries = 0
		sleepfor = 0.1
		while tries < self.maxretry:
			try:
				socket = self.get_idle_socket()
				socket.send(msgpack.packb([args, kwargs]))
				response = msgpack.unpackb(socket.recv())
				self.recycle_socket(socket)
				if 'error' in response:
					raise RemoteException(*response['error'])
				return response['ok']
			except zmq.ZMQError as e:
				# dang, we experienced a ZMQ related error, try again
				gevent.sleep(sleepfor)
				self.maxsock += 1
				sleepfor *= 2
				tries += 1
				pass
		raise zmq.ZMQError('could not make request after %d tries'%(tries))

	def a(self, *args, **kwargs):
		'''
		return a greenlet that can be used to asynchronously access
		the request response.
		'''
		return gevent.spawn(self.request, *args, **kwargs)
		
	def __call__(self, *args, **kwargs):
		return self.request(*args, **kwargs)


class ProxyClient(object):
	pass

class RouterClient(object):

	def __init__(self, address):
		self.address = address
		
	def request(self, route, *args, **kwargs):
		return azmq.request(self.address, route, *args, **kwargs)

	def __call__(self, route, *args, **kwargs):
		return self.request(route, *args, **kwargs)

	@property
	def routes(self):
		routes = {}
		for route in self.request('info|routes'):
			def forwarder(*args, **kwargs):
				return self.request(route, *args, **kwargs)
			routes[route] = forwarder
		return routes




