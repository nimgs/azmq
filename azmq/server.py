
import gevent
import msgpack
import zmq.green as zmq

from gevent.pool import Pool

class Server(object):
	'''
	Start a ZMQ based server at specified `address` and pass off incoming
	messages of the form `[args, kwargs]` to the supplied `handler` as
	`handler(*args, **kwargs)`.

	`handler` should be a gevent aware function if it does IO, so that
	the main server loop can handle other concurrent requests.  If your
	function is not gevent aware and is CPU bound, look into wrapping it
	in a gevent aware `Process` from the gmp module.

	To control the number of open connections being handled, the server
	spawns all of its handlers from a gevent Pool.  The size of this
	pool is controlled by `concurrency`.  This helps prevent exhaustion
	of system resources, such as open file handles.
	'''

	manager = None
	socket  = None
	
	def __init__(self, address, handler, concurrency=1000):
		self.address = address
		self.handler = handler
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.ROUTER)
		self.pool = Pool(concurrency)
		self.start()
	
	def request_loop(self):
		'''
		starts an infinite loop that handles messages coming over the zmq socket
		and spawns the request handling in a new greenthread so that it can
		return as quickly as possible to fetching the next zmq message.
		'''
		while True:
			# block the current greenlet until we get a zmq message
			message = self.socket.recv_multipart()
			# as soon as we get a message, spawn a new greenlet
			# to process the message and respond
			def respond(message):
				# split zmq routing info and unpack msgpacked arguments
				routing = message[:-1]
				args, kwargs = msgpack.unpackb(message[-1])
				result = {}
				try:
					# pass arguments to handler and set result
					result['ok'] = self.handler(*args, **kwargs)
					if isinstance(result['ok'], gevent.Greenlet):
						# go ahead and resolve a greenlet value...
						# we can't serialize a greenlet anyways
						result['ok'] = result['ok'].get()
				except Exception as e:
					# uh oh, handler raised an exception.  pass some info
					# back to the client to give them a heads up
					import traceback
					result['error'] = (repr(e), traceback.format_exc())
				# msgpack result and reattach zmq routing info
				response = routing + [msgpack.packb(result)]
				self.socket.send_multipart(response)
			# spawn the response thread in a new greenlet controlled
			# by the pool.  If greenlet pool is exhausted, this 
			# blocks the request_loop until one opens up... zmq buffers
			# any messages that pile during this time
			self.pool.spawn(respond, message)
	
	def start(self):
		'''
		bind the server's socket to an address and start its
		request handling loop in a new greenlet
		'''
		if self.manager:
			raise RuntimeError('attempt to start server twice')
		self.manager = gevent.spawn(self.request_loop)
		self.socket.bind(self.address)

	def join(self, *args, **kwargs):
		self.manager.join(*args, **kwargs)

		



