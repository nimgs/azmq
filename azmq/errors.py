

class RemoteException(BaseException):
    
    def __init__(self, exception, traceback):
        self.exception = exception
        self.traceback = traceback
    
    def __repr__(self):
        string  = "\n"
        string += "----------------------------------------\n"
        string += "Remote Exception: %s\n"%(self.exception)
        string += "       Traceback:\n"
        string += self.traceback
        string += "----------------------------------------\n"
        return string

    def __str__(self):
    	return repr(self)
