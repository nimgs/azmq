#!/usr/bin/env python

from setuptools import setup

setup( name='azmq',
       version='1.0.0',
       #install_requires=['pyzmq', 'msgpack'],
       packages=['azmq'],
)
