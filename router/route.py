
def split(route):
	'''
	>>> split('get|name.path')
	('get', ['name', 'path'])
	>>> split('get')
	('get', [])
	>>> split('get|bad|name.path')
	Traceback (most recent call last):
	...
	ValueError: `get|bad|name.path` is not a valid route
	'''
	parts = route.split('|')
	if len(parts) == 1:
		return parts[0], []
	elif len(parts) == 2:
		return parts[0], split_path(parts[1])
	else:
		raise ValueError('`%s` is not a valid route'%(route))

def join(method, *pathc):
	'''
	>>> join('get', 'name', 'path')
	'get|name.path'
	>>> join('get')
	'get'
	'''
	if len(pathc):
		return '%s|%s'%(method, '.'.join(pathc))
	return method

def prefix(route, prefix):
	'''
	>>> prefix('get|path', 'name')
	'get|name.path'
	>>> prefix('get', 'name')
	'get|name'
	'''
	method, pathc = split(route)
	return join(method, prefix, *pathc)
	
def unprefix(route, prefix):
	'''
	>>> unprefix('get|name.path', 'name')
	'get|path'
	>>> unprefix('get|name.path', '')
	'get|name.path'
	>>> unprefix('get|name.path', 'x')
	Traceback (most recent call last):
	...
	ValueError: route `get|name.path` does not contain prefix `x`
	'''
	if prefix_matches(prefix, route):
		method, pathc = split(route)
		prefixc = split_path(prefix)
		return join(method, *pathc[len(prefixc):])
	raise ValueError('route `%s` does not contain prefix `%s`'%(route, prefix))

def split_path(path):
	if path == '':
		return []
	return path.split('.')

def prefix_matches(prefix, route):
	if prefix == '':
		return True
	prefixc = split_path(prefix)
	method, pathc = split(route)
	if len(prefixc) <= len(pathc):
		if prefixc == pathc[:len(prefixc)]:
			return True
	return False

if __name__ == "__main__":
    import doctest
    doctest.testmod()
