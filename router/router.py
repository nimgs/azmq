
import route

class Router(object):

	def __init__(self, address):
		self.routes = {}

	def add(self, verb, noun, handler):
		self.routes[verb][noun] = handler

	def attach(self, routes, prefix=None):
		for verb, nouns in routes.items():
			for noun, handler in nouns.items():
				if prefix:
					noun = "%s.%s"%(prefix, noun)
				self.add(verb, noun, handler)

	def route(self, verb, noun, body):
		return self.routes[verb][noun](body)

	def __call__(self, *args, **kwargs):
		self.route(*args, **kwargs)

	





