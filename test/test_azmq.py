
import unittest

class Test(unittest.TestCase):

	def setUp(self):
		import azmq
		import random
		address = 'tcp://127.0.0.1:%d'%(random.randint(6000,6500))
		self.server = azmq.Server(address, self.echo, concurrency=500)
		self.client = azmq.Client(address, concurrency=200)

	def echo(self, number):
		return number

	def test_async(self):
		numbers  = list(range(10))
		promises = map(self.client.a, numbers)
		results  = [p.get() for p in promises]
		if any([r != i for r, i in zip(results, numbers)] ):
			raise RuntimeError("value mismatch")

	def test_sync(self):
		numbers = list(range(10))
		results = map(self.client.request, numbers)
		if any([r != i for r, i in zip(results, numbers)] ):
			raise RuntimeError("value mismatch")

